from math import sin, floor
import noise
from modelo.medicion import agregarMedicion, obtenerMediciones
from modelo.lugar import agregarLugar

def get_values( cant, base, off, scale ) :
	x = []
	y = []

	for i in range( cant ) :
		x.append( i )

		v = 0
		for j in range( len( off ) ) :
			v += noise.pnoise1(
				i * scale[j] + off[j],
				octaves = 5,
				persistence = 0.5,
				lacunarity = 2
			)

		y.append( base + v )

	return x, y

#vel_viento, dir, mm, temp, humedad, presion
def cargar( cant ) :
	idHL = agregarLugar( 'Hacklab', -32.317886, -58.086541 )
	idPA = agregarLugar( 'Plaza Artigas', -32.315760, -58.094936 )
	idE98 = agregarLugar( 'Escuela 98', -32.320493, -58.059569 )

	for j in [ idHL, idPA, idE98 ] :
		x, vel = get_values( cant, 10 + j, [10 + j, 2 + j], [.01, .1] )
		x, temp = get_values( cant, 24 + j, [145 + j, 23 + j, 12 + j], [.1, .2, 5] )
		x, hum = get_values( cant, 38 + j, [2 + j, 4 + j, 123 + j], [.21354, .4325, 52438] )
		x, pres = get_values( cant, 1000 + j, [15 + j, 153 + j, 45 + j], [.135, .453, .453] )
		x, mm = get_values( cant, 10 + j, [10 + j, 2 + j], [.01, .1] )
		mm = [ i * 100 if i > 10.1 else 0 for i in mm ]

		for i in range( cant ) :
			agregarMedicion(
				j,
				float( '%.2f' % vel[i] ),
				float( '%.2f' % ( ( sin( i / 10 ) + 1 ) / 2 * 360 ) ),
				float( '%.2f' % mm[i] ),
				float( '%.2f' % temp[i] ),
				float( '%.2f' % hum[i] ),
				float( '%.2f' % pres[i] ),
				get_fecha( i )
			)


def get_fecha( i ) :
	return( '2020-01-' + str( floor( 20 + ( i / 48 ) ) ) + ' ' + str( floor( ( i % 48 ) / 2 ) ) + ( ':00:00' if i % 2 == 0 else ':30:00' ) )