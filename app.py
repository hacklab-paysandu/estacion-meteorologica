import os
if os.path.exists( 'bd.sqlite' ) :
	os.remove( 'bd.sqlite' )
from datos_prueba import cargar
cargar( 230 )

from flask import Flask, request, render_template, Markup
from modelo.medicion import agregarMedicion, obtenerMediciones
from modelo.lugar import obtenerLugares
import json

app = Flask( __name__ )

@app.route( '/' )
def main() :
	return render_template( 'index.html' )


@app.route( '/agregarMedicion' ) # TODO: hange to POST
def flask_agregarMedicion() :
	agregarMedicion( 1, 2, 3, 4, 5, 6 )
	return res( 'OK' )


@app.route( '/obtenerMediciones' )
def flask_obtenerMediciones() :
	lugar = request.args.get( 'nombreLugar', '' )

	if lugar == '' :
		return res( 'ERR', 'Ingrese nombre del lugar' )
	else :
		mediciones = obtenerMediciones( lugar )
		return res( 'OK', mediciones )


@app.route( '/obtenerLugares' )
def flask_obtenerLugares() :
	return res( 'OK', obtenerLugares() )


def res( estado, msj = '' ) :
	return { 'estado': estado, 'mensaje': msj }


if __name__ == '__main__' :
	app.run( threaded = True, port = 5000 )
