$( document ).ready( () => {
	// Cuando el documento está listo, se carga el mapa
	prepararMapa()

	document.getElementById( 'seleccion' ).innerText = 'Mostrando todo'

	// Botón volver
	document.getElementById( 'volver' ).onclick = () => {
		volverAlMapa()
	}

	// Botón seleccionar día
	document.getElementById( 'mostrar' ).onclick = () => {
		cargarDatosDelDiaSeleccionado()
	}

	// Botón mostrar todo
	document.getElementById( 'mostrar_todo' ).onclick = () => {
		cargarDatosGuardados()
		document.getElementById( 'seleccion' ).innerText = 'Mostrando todo'
	}

	document.getElementById( 'fecha' ).value = getFecha()
	document.getElementById( 'fecha' ).max = getFecha()
} )

// Lugar y zoom inicial del mapa
let latLngInicial = [-32.316993, -58.076493]
let zoomInicial = 14

let mapa = undefined
prepararMapa = () => {
	// Se crea el mapa
	mapa = L.map( 'mapa' ).setView( latLngInicial, zoomInicial )

	let marcador = undefined
	mapa.on( 'click', function ( e ) {
		if ( marcador == undefined )
			marcador = L.marker( e.latlng ).addTo( mapa )
		else
			marcador.setLatLng( e.latlng )
	} )

	// Se asigna un proveedor de imagenes
	L.tileLayer(
		'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
		{
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			maxZoom: 18,
			id: 'mapbox/streets-v11',
			tileSize: 512,
			zoomOffset: -1,
			accessToken: 'pk.eyJ1Ijoiam9yZ2UxNzAxIiwiYSI6ImNrNzUwN2wyczBybG8zbnFxc2JzbHM0dDIifQ.iVC9lbpnRBnNkZLslfbIog'
		}
	).addTo( mapa )

	// Se obtienen los lugares del servidor
	$.ajax( {
		url: '/obtenerLugares',
		success: ( res ) => {
			if ( res.estado == 'OK' )
			{
				// Se cargan los lugares como marcadores
				res.mensaje.map( l => {
					let lugar = L.marker( [l.lat, l.lng] )
						.bindTooltip( l.nombre, { permanent: true, direction: 'top' } )
						.addTo( mapa )

					// Al hacer click en un marcador se cargan los datos de este lugar
					lugar.on( 'click', ( e ) => {
						cargarDatosDeLugar( e.target.getTooltip().getContent() )
					} )
				} )
			}
			else
				console.log( 'Error' )
		}
	} )
}

volverAlMapa = () => {
	// Se agranda el mapa
	$( '#mapa_div' ).animate( { height: '100vh' }, 250 )
	// Se oculta el header
	$( '#header' ).animate( { top: '-50px' }, 250 )
}

cargarDatosDeLugar = ( nombreLugar ) => {
	// Se obtiene la información de ese lugar
	$.ajax( {
		url: '/obtenerMediciones?nombreLugar=' + nombreLugar,
		success: ( res ) => {
			if ( res.estado == 'OK' )
			{
				// Ocultar mapa
				$( '#mapa_div' ).animate( { height: '0px' }, 250 )
				// Mostrar header
				$( '#header' ).animate( { top: '0px' }, 250 )
				$( '#nombre_lugar' ).text( nombreLugar )

				mostrarDatos( res.mensaje )
			}
			else
				console.log( 'Error' )
		}
	} )
}

let fechas = []
let direccion = []
let humedad = []
let mm = []
let presion = []
let temperatura = []
let velocidad = []

mostrarDatos = ( datos ) => {
	fechas = []
	direccion = []
	humedad = []
	mm = []
	presion = []
	temperatura = []
	velocidad = []

	datos.forEach( dato => {
		fechas.push( dato['fecha'] )
		direccion.push( dato['dir'] )
		humedad.push( dato['humedad'] )
		mm.push( dato['mm'] )
		presion.push( dato['presion'] )
		temperatura.push( dato['temp'] )
		velocidad.push( dato['vel_viento'] )
	} )

	cargarDatosGuardados()
}

cargarDatosGuardados = () => {
	cargar( fechas, temperatura, humedad, mm, presion )
}

cargar = ( fechas, temperatura, humedad, mm, presion ) => {
	crearGrafica( 'grafTemperatura', 'Temperatura', fechas, temperatura, 'rgb(255, 105, 182)' )
	crearGrafica( 'grafHumedad', 'Humedad', fechas, humedad, 'rgb(255, 206, 82)' )
	crearGrafica( 'grafMm', 'Milímetros', fechas, mm, 'rgb(66, 176, 245)' )
	crearGrafica( 'grafPresion', 'Presión', fechas, presion, 'rgb(43, 255, 153)' )
}

let graficas = {}
crearGrafica = ( idElemento, titulo, labels, data, color ) => {
	if ( graficas[idElemento] != undefined )
	{
		graficas[idElemento].data.labels = labels
		graficas[idElemento].data.datasets[0].data = data

		graficas[idElemento].update()
	}
	else
	{
		let graf = document.getElementById( idElemento ).getContext( '2d' )
		let grafica = new Chart( graf, {
			type: 'line',
			data: {
				labels: labels,
				datasets: [
					{
						label: titulo,
						backgroundColor: color,
						borderColor: color,
						data: data,
						fill: false
					}
				]
			},
			options: {}
		} )

		graficas[idElemento] = grafica
	}
}

cargarDatosDelDiaSeleccionado = () => {
	let seleccion = document.getElementById( 'fecha' ).value
	document.getElementById( 'seleccion' ).innerText = seleccion

	let nuevas_fechas = []
	let is = []

	for ( let i = 0; i < fechas.length; i++ )
		if ( fechas[i].split( ' ' )[0] == seleccion )
		{
			nuevas_fechas.push( fechas[i].split( ' ' )[1] )
			is.push( i )
		}

	
	let n_temperatura = is.map( i => temperatura[i] )
	let n_humedad = is.map( i => humedad[i] )
	let n_presion = is.map( i => presion[i] )
	let n_mm = is.map( i => mm[i] )

	cargar(
		nuevas_fechas,
		n_temperatura,
		n_humedad,
		n_mm,
		n_presion
	)
}

getFecha = () => {
	let d = new Date()

	let dia = d.getDate()
	dia = dia.toString().length == 1 ? '0' + dia : dia
	let mes = d.getMonth()
	mes = mes.toString().length == 1 ? '0' + mes : mes
	let anio = d.getFullYear()

	return anio + '-' + mes + '-' + dia
}

/*$.ajax( {
	type: 'POST',
	url: '/registrarCasa',
	data: {
		'lat': marcador.lat,
		'lng': marcador.lng
	},
	success: ( res ) => {
		
	},
	error: ( err ) => {

	}
} )*/