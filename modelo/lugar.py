from bd import connection

def agregarLugar( nombre, lat, lng ) :
	conn, c = connection()

	c.execute(
		'INSERT INTO lugares ( nombre, lat, lng ) VALUES ( ?, ?, ? )',
		( nombre, lat, lng )
	)

	conn.commit()
	conn.close()

	return c.lastrowid


def obtenerLugares() :
	conn, c = connection()

	lugares = []
	for row in c.execute( 'SELECT * FROM lugares' ) :
		lugares.append( {
			'id': row[0],
			'nombre': row[1],
			'lat': row[2],
			'lng': row[3]
		} )

	return lugares
