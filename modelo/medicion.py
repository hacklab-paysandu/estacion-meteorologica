from bd import connection
from datetime import datetime

def agregarMedicion( id_lugar, vel_viento, dir, mm, temp, humedad, presion, fecha = None ) :
	conn, c = connection()

	fecha_hora = fecha if fecha != None else datetime.now().strftime( '%Y-%m-%d %H:%M:%S' )
	c.execute(
		'INSERT INTO mediciones ( fecha_hora, vel_viento, dir, mm, temp, humedad, presion, id_lugar ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )',
		( fecha_hora, vel_viento, dir, mm, temp, humedad, presion, id_lugar )
	)

	conn.commit()
	conn.close()


def obtenerMediciones( lugar ) :
	conn, c = connection()

	mediciones = []
	for row in c.execute( 'SELECT * FROM mediciones AS m, lugares AS l WHERE m.id_lugar = l.id AND l.nombre = ?', ( lugar, ) ) :
		mediciones.append( {
			'id': row[0],
			'fecha': row[1],
			'vel_viento': row[2],
			'dir': row[3],
			'mm': row[4],
			'temp': row[5],
			'humedad': row[6],
			'presion': row[7],
		} )

	conn.close()
	return mediciones
