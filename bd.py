import sqlite3

BD = 'bd.sqlite'

def connection() :
	conn = sqlite3.connect( BD )
	c = conn.cursor()
	return conn, c


conn, c = connection()
c.execute(
	'''CREATE TABLE IF NOT EXISTS mediciones (
		id INTEGER PRIMARY KEY,
		fecha_hora TEXT,
		vel_viento REAL,
		dir REAL,
		mm REAL,
		temp REAL,
		humedad REAL,
		presion REAL,
		id_lugar INTEGER
	)'''
)

c.execute(
	'''CREATE TABLE IF NOT EXISTS lugares (
		id INTEGER PRIMARY KEY,
		nombre TEXT,
		lat REAL,
		lng REAL
	)'''
)

conn.commit()
conn.close()
